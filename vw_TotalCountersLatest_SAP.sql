SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW dbo.vw_TotalCountersLatest_SAP
AS
SELECT        M.MACH_ID, M.SERIAL_NO, M.CUST_ID, CASE WHEN LEN(M.DCA_ID) 
                         > 2 THEN 'DCA' ELSE (CASE M.COMMTYPE WHEN '2' THEN 'EMAIL' WHEN '6' THEN 'HTTP1' WHEN '7' THEN 'HTTP2' WHEN '9' THEN 'GPRS' WHEN '13' THEN 'DCA' ELSE 'OTHER' END) 
                         END AS COMM_PROTOCOL, M.COMSVR_ID, M.ERP_MACH_ID, CONVERT(varchar(50), M.LATEST_RECEIVED, 121) AS LATEST_RECEIVED, CONVERT(varchar(50), M.MOUNTED_DATE, 121) AS MOUNTED_DATE, 
                         M.MANAGE, b.CUST_NAME, c.OFC_NAME, e.CENTER_ID, d.MODEL_NAME, f.CNTR_NAME, CONVERT(varchar(50), dbo.tblTotalCountersLatest.RECEIVED, 121) AS TOTAL_REG_DATE, CONVERT(varchar(50), 
                         dbo.tblTotalCountersLatest.RECEIVED, 121) AS SIZE_REG_DATE, '||' AS KeyDataEnd, 
							CASE dbo.tblTotalCountersLatest.TOTAL WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.TOTAL END AS total_counter, 
							CASE dbo.tblTotalCountersLatest.PRINTER_TOTAL WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_TOTAL END AS print_counter, 
							CASE dbo.tblTotalCountersLatest.TOTAL WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.TOTAL END AS TOTAL, 
							CASE dbo.tblTotalCountersLatest.DUPLEX_TOTAL WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.DUPLEX_TOTAL END AS [Duplex:Total], 
							CASE dbo.tblTotalCountersLatest.MANUSCRIPT WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.MANUSCRIPT END AS MANUSCRIPT, 
							CASE dbo.tblTotalCountersLatest.DISCHARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.DISCHARGE END AS DISCHARGE, 
                         CASE dbo.tblTotalCountersLatest.COPY_TOTAL WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_TOTAL END AS [Copy:Total], 
							CASE dbo.tblTotalCountersLatest.COPY_BLACK WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_BLACK END AS [Copy:Black], 
							CASE dbo.tblTotalCountersLatest.COPY_FULL_COLOR WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_FULL_COLOR END AS [Copy:Full Color], 
                         CASE dbo.tblTotalCountersLatest.COPY_MONO_COLOR WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_MONO_COLOR END AS [Copy:Mono Color], 
							CASE dbo.tblTotalCountersLatest.COPY_2C_COLOR WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_2C_COLOR END AS [Copy:2C Color], 
							CASE dbo.tblTotalCountersLatest.COPY_TOTAL_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_TOTAL_LARGE END AS [Copy:Total Large], 
                         CASE dbo.tblTotalCountersLatest.COPY_BLACK_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_BLACK_LARGE END AS [Copy:Black Large], 
							CASE dbo.tblTotalCountersLatest.COPY_FULL_COLOR_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_FULL_COLOR_LARGE END AS [Copy:Full Color Large], 
                         CASE dbo.tblTotalCountersLatest.COPY_MONO_COLOR_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_MONO_COLOR_LARGE END AS [Copy:Mono Color Large], 
							CASE dbo.tblTotalCountersLatest.COPY_2C_COLOR_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.COPY_2C_COLOR_LARGE END AS [Copy:2C Color Large], 
                         CASE dbo.tblTotalCountersLatest.PRINTER_TOTAL WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_TOTAL END AS [Printer:Total], 
							CASE dbo.tblTotalCountersLatest.PRINTER_BLACK WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_BLACK END AS [Printer:Black], 
							CASE dbo.tblTotalCountersLatest.PRINTER_FULL_COLOR WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_FULL_COLOR END AS [Printer:Full Color], 
                         CASE dbo.tblTotalCountersLatest.PRINTER_2C_COLOR WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_2C_COLOR END AS [Printer:2C Color], 
							CASE dbo.tblTotalCountersLatest.PRINTER_TOTAL_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_TOTAL_LARGE END AS [Printer:Total Large], 
                         CASE dbo.tblTotalCountersLatest.PRINTER_BLACK_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_BLACK_LARGE END AS [Printer:Black Large], 
							CASE dbo.tblTotalCountersLatest.PRINTER_FULL_COLOR_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_FULL_COLOR_LARGE END AS [Printer:Full Color Large], 
                         CASE dbo.tblTotalCountersLatest.PRINTER_2C_CLOR_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.PRINTER_2C_CLOR_LARGE END AS [Printer:2C Color Large], 
							CASE dbo.tblTotalCountersLatest.SCANNER_FAX_SCAN WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.SCANNER_FAX_SCAN END AS [Scanner/FAX:Scan], 
                         CASE dbo.tblTotalCountersLatest.SCANNER_FAX_SCAN_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.SCANNER_FAX_SCAN_LARGE END AS [Scanner/FAX:Scan Large], 
							CASE dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT END AS [Scanner/FAX:Print], 
                         CASE dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT_LARGE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT_LARGE END AS [Scanner/FAX:Print Large], 
                         CASE dbo.tblTotalCountersLatest.FAX_SEND WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.FAX_SEND END AS [FAX:Send], 
                         CASE dbo.tblTotalCountersLatest.FAX_RECEIVE WHEN - 1 THEN '0' ELSE dbo.tblTotalCountersLatest.FAX_RECEIVE END AS [FAX:Receive], 
                         CASE WHEN dbo.tblTotalCountersLatest.TOTAL - dbo.tblTotalCountersLatest.BLACK_TOTAL < 1 THEN '0' ELSE dbo.tblTotalCountersLatest.TOTAL - dbo.tblTotalCountersLatest.BLACK_TOTAL END AS [Full Color:Total],
                         dbo.tblTotalCountersLatest.BLACK_TOTAL AS [Black:Total], 
							dbo.tblTotalCountersLatest.TWO_C_COLOR_TOTAL AS [2C Color:Total], 
                         dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT_BLACK AS [Scanner/FAX:Print(Black)], 
							dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT_LARGE_BLACK AS [Scanner/FAX:Print Large(Black)], 
                         dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT_FULL_COLOR AS [Scanner/FAX:Print(Full Color)], 
                         dbo.tblTotalCountersLatest.SCANNER_FAX_PRINT_LARGE_FULL_COLOR AS [Scanner/FAX:Print Large(Full Color)], 
							dbo.tblTotalCountersLatest.W_COUNT_SETTING AS [W Count Setting], 
                         dbo.tblTotalCountersLatest.BILLING_TOTAL AS [Billing total counter], 
							dbo.tblTotalCountersLatest.BILLING_TOTAL_COLOR AS [Billing total counter(Color)], 
                         dbo.tblTotalCountersLatest.BILLING_TOTAL_BLACK AS [Billing total counter(Black)], '||' AS TotalsDataEnd, '0' AS totalCounter, '0' AS printerTotal, '0' AS Others, '0' AS A3, '0' AS [A4 SEF], '0' AS A5, '0' AS A6, 
                         '0' AS B4, '0' AS [B5 SEF], '0' AS B6, '0' AS [12x18], '0' AS [11x17], '0' AS [8.5X14], '0' AS [8.5x11 SEF], '0' AS [7.25x10.5], '0' AS [5.5x8.5], '0' AS F4, '0' AS Postcard, '0' AS [4x6], '0' AS [8K], '0' AS [16K], 
                         '0' AS LongPaper, '0' AS A2, '0' AS SRA3, '0' AS SRA4, '0' AS [13X19], '0' AS Infinite1, '0' AS Infinite2, '0' AS Infinite3, '0' AS Infinite4, '0' AS Infinite5, '0' AS [A4 LEF], '0' AS [B5 LEF], '0' AS [8.5x11 LEF], 
                         '0' AS [Total count(Total copy count of each paper size)], '0' AS [A4/8.5x11-like Color Count], '0' AS [A4/8.5x11-like B&W Count], '0' AS [A3/11x17-like Color Count], '0' AS [A3/11x17-like B&W Count], 
                         '0' AS [Infinete1 Threshold], '0' AS [Infinete2 Threshold], '0' AS [Infinete3 Threshold], '0' AS [Infinete4 Threshold], '||' AS SizeEnd, dbo.vw_MaxBillingCounter.[Billing Color:Small:Band1], 
                         dbo.vw_MaxBillingCounter.[Billing Color:Banner:Band1-P], dbo.vw_MaxBillingCounter.[Billing Color:A4:Band1], dbo.vw_MaxBillingCounter.[Billing Color:Large:Band1], 
                         dbo.vw_MaxBillingCounter.[Billing Color:Banner:Band1], dbo.vw_MaxBillingCounter.[Billing Color:Small:Band1-P], '||' AS BillDataEnd
FROM            dbo.MACHINEINFO AS M LEFT OUTER JOIN
                         dbo.tblTotalCountersLatest ON dbo.tblTotalCountersLatest.MACH_ID = M.MACH_ID AND M.DEL_FLG = 0 LEFT OUTER JOIN
                         dbo.vw_MaxBillingCounter ON dbo.vw_MaxBillingCounter.MACH_ID = M.MACH_ID LEFT OUTER JOIN
                         dbo.CUSTOMERINFO AS b ON b.CUST_ID = M.CUST_ID LEFT OUTER JOIN
                         dbo.OFFICEINFO AS c ON c.OFC_ID = M.SRV_ID LEFT OUTER JOIN
                         dbo.MODELINFO AS d ON d.MACHINE_TYPE = M.MACH_TYPE LEFT OUTER JOIN
                         dbo.COM_DEVICE AS e ON e.DEVICE_ID = M.MACH_ID LEFT OUTER JOIN
                         dbo.CENTERINFO AS f ON f.CNTR_ID = M.CNTR_ID
WHERE        (M.DEL_FLG = 0)